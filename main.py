from tkinter import *
import numpy as np
import random
import sys

canvas_width = 700
canvas_height = 700
canvas = None
root = None

counter = 0

m = 10 # numero de pontos
n = 10 # numero de nos

x_array= []
y_array = []

def donothing():
    x = 0

def B(t):
    if (t < -2.0):
        return 0
    elif (t < -1.0):
        y = __p0(t)
    elif (t < 0.0):
        y = __p1(t)
    elif (t < 1.0):
        y = __p2(t)
    elif (t < 2.0):
        y = __p3(t)
    else:
        return 0
    return y

def __p0(t):
    return (t + 2)**3/4

def __p1(t):
    return -(3/4)*(t**3) - (3/2 * t**2) + 1

def __p2(t):
    return (3/4)*(t**3) - (3/2 * t**2) + 1

def __p3(t):
    return (2 - t)**3/4

def B_twoline(t):
    if (t < -2.0):
        return 0
    elif (t < -1.0):
        y = __p0_twoline(t)
    elif (t < 0.0):
        y = __p1_twoline(t)
    elif (t < 1.0):
        y = __p2_twoline(t)
    elif (t < 2.0):
        y = __p3_twoline(t)
    else:
        return 0
    return y

def __p0_twoline(t):
    return 3/2 * (t + 2)

def __p1_twoline(t):
    return -9/2 * (t) - 3

def __p2_twoline(t):
    return 9/2 * (t) - 2

def __p3_twoline(t):
    return 3/2 * (t - 2)

def simpson(i, j, n):
    result = 0.0
    interval = 1
    for k in range(0, n, interval):
        a  = k
        b = a + interval
        f_a = B_twoline(a - i) * B_twoline(a - j)
        t = (a + b) / 2
        f_b = B_twoline(t - i) * B_twoline(t - j)
        f_c = B_twoline(b - i) * B_twoline(b - j)
        h = (b - a) / 2
        result += ((f_a + 4.0 * f_c + f_b)) * (h / 3.0)
    return 2 * result

def clean_up():
    global canvas, root, x_array, y_array, counter
    canvas.delete("all")
    root.bind("<Button 1>", donothing)
    x_array = []
    y_array = []
    counter = 0

def getPoints(eventorigin):
    global m, counter, x_array, y_array

    if counter < m:
        x_array.append(eventorigin.x)
        y_array.append(eventorigin.y)
        counter += 1
    
    if counter == m:
        smoothing_spline(x_array, y_array)
        x_array = []
        y_array = []
        counter = 0

    canvas.create_oval(eventorigin.x, eventorigin.y, eventorigin.x, eventorigin.y, width=5, fill="#FF0000")

def scale(array):
    return np.dot(1/70, array)

def smoothing_spline(x_array, y_array):
    global canvas, m, n

    x_array = scale(x_array)
    y_array = scale(y_array)

    B_matrix = np.zeros((m, n))

    for i in range(m):
        for j in range(n):
            B_matrix[i][j] = B(x_array[i] - j)

    bTb = np.dot(B_matrix.T, B_matrix)

    M_matrix = np.zeros((n, n))

    for i in range(n):
        for j in range(n):
            M_matrix[i][j] = simpson(i, j, 10)

    bTy = np.dot(B_matrix.T, y_array)

    l = 0.01
    lM = np.dot(l, M_matrix)
    a = np.dot(np.linalg.inv((bTb + lM)), bTy)

    t = x_array[0]
    while (t < x_array[-1]):
        y = 0
        for i in range(n):
            aux = a[i] * B(t - i)
            y += aux

        canvas.create_oval(t * 70, y * 70, t * 70, y * 70, width=0, fill="#0000FF")
        t += 0.01

def bspline_points():
    global canvas, m
    x_array = []
    y_array = []

    t = 0

    # coeficientes
    a_array = []
    for i in range(10):
        a_array.append(random.random())

    counter = 0
    cut_point = 1000//m
    while (t < 10):
        y = 0
        for i in range(10):
            aux = a_array[i] * B(t - i)
            y += aux

        counter += 1
        if (counter == cut_point):
            x_array.append(t * 70)
            y_array.append((y * 70) + random.uniform(-10,10))
            counter = 0
            canvas.create_oval(x_array[-1], y_array[-1],x_array[-1] , y_array[-1], width=3, fill="#FF0000")
        canvas.create_oval((t) * 70, y * 70, (t) * 70, y * 70, width=0, fill="#FF0000")
        t += 0.01

    smoothing_spline(x_array, y_array)

def your_points():
    clean_up()
    root.bind("<Button 1>", getPoints)

def random_spline():
    clean_up()
    root.bind("<Button 1>", donothing)
    bspline_points()

def main():
    global canvas_width, canvas_height, canvas, root, m

    if len(sys.argv) >= 2:
        m = int(sys.argv[1])

    # tk init
    root = Tk()
    root.title("Smoothing Splines")

    # Canvas
    canvas = Canvas(root, 
           width=canvas_width,
           height=canvas_height,
           bg="#FFFFFF")
    canvas.pack()

    # Put Menu
    menubar = Menu(root)
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="Escolha os pontos para sua Smoothing Spline", command=your_points)
    filemenu.add_command(label="Smoothing Spline a partir de pontos de um bspline aleatória", command=random_spline)
    filemenu.add_command(label="Limpar", command=clean_up)
    filemenu.add_separator()
    filemenu.add_command(label="Sair", command=root.quit)
    menubar.add_cascade(label="Opções", menu=filemenu)
    root.config(menu=menubar)
    root.mainloop()

main()