# EP2 de MAC0210

### Eduardo Pinheiro e Gabriely Pereira

Escreva uma função que calcula os coeficientes $`a_i`$ da *B-spline* cúbica:

```math
s_a(t) = \sum_{i=0}^{n} a_i \beta(t - i)
```
Que minimiza o críterio:

```math
f(a) = \sum_{\ell=0}^{m} (s_a(t_\ell) - y_\ell)^2 + \lambda \int_{0}^{n} (s''(t))^2 dt
```